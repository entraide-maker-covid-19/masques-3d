# Description générale

Masque dont la partie en contact avec la peau est imprimé en mode vase

## Avantage :
+ Permet un certain confort (léger et souple) avec un masque en PLA
+ Impression rapide
+ Porte filtre modulable
+ Réutilisable
+ Possibilité de filtre HEPA ou autre (sac aspirateur, tissu etc etc)

## Inconvénient :
+ Peut être fragile (surtout si impression mal maitrisé ou PLA humide)

# Notice Fabrication

	A définir

# Notice d'utilisation

	A définir

# Temps d'impression

	Voir le fichier Temps_de_fab.ods